//
//  DataModel.swift
//  emilymobile
//
//  Created by pbanavara on 13/04/16.
//  Copyright © 2016 pbanavara. All rights reserved.
//

import Foundation

class DataModel {
    
    static let sharedInstance = DataModel()
    var listOfFilesToBeUploaded: [Dictionary<String, AnyObject>] = []
    
    private init() {
     
    }
    
}
