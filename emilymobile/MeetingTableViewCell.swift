//
//  MeetingTableViewCell.swift
//  emilymobile
//
//  Created by pbanavara on 04/04/16.
//  Copyright © 2016 pbanavara. All rights reserved.
//

import UIKit

class MeetingTableViewCell: UITableViewCell {

    @IBOutlet weak var meetingNumber: UILabel!
    @IBOutlet weak var uploadTextLabel: UILabel!
    @IBOutlet weak var uploadProgressView: UIProgressView!
    @IBOutlet weak var viewTranscript: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var meetingRecordingDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
