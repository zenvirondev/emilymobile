//
//  ListOfMeetingsControllerTableViewController.swift
//  emilymobile
//
//  Created by pbanavara on 04/04/16.
//  Copyright © 2016 pbanavara. All rights reserved.
//

import UIKit
import AVKit

class ListOfMeetingsTableViewController: UITableViewController {
    lazy var searchBar = UISearchBar(frame: CGRectMake(0, 0, 0, 0))
    var uploadFlag:Bool = false
    var currentFileName:String!
    var transcriptIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.placeholder = "Search"
        navigationItem.titleView = searchBar
        tableView.separatorColor = UIColor.blackColor()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "methodOFReceivedNotication:", name:"ShowTranscript", object: nil)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return DataModel.sharedInstance.listOfFilesToBeUploaded.count
    }
  
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->
        UITableViewCell {
            let cell = tableView.dequeueReusableCellWithIdentifier("meetingCell", forIndexPath: indexPath) as! MeetingTableViewCell
            let row = indexPath.row
            
            let fileDisplayString = (DataModel.sharedInstance.listOfFilesToBeUploaded[row]["fileName"] as! NSString).description
            let fs = fileDisplayString.characters.split{$0 == "."}.map(String.init)
            cell.meetingRecordingDate.text = fs[0]
            if ((DataModel.sharedInstance.listOfFilesToBeUploaded[row]["s3url"] as! NSString).description != "") {
                cell.viewTranscript.setTitle("View Transcript", forState: .Normal)
            }
            cell.viewTranscript.addTarget(self, action: "viewTranscript:", forControlEvents: .TouchUpInside)
            //cell.playButton.addTarget(self, action: "playAudio:", forControlEvents: .TouchUpInside)
            
            cell.uploadProgressView.hidden = true
            cell.uploadTextLabel.hidden = true
            cell.meetingNumber.text = "Meeting \(row+1)"
            return cell
    }
    
    func viewTranscript(sender:UIButton) {
        let point = tableView.convertPoint(CGPointZero, fromView: sender)
        let indexPath = tableView.indexPathForRowAtPoint(point)!
        if (sender.titleLabel?.text == "Upload File") {
                let fileName = (DataModel.sharedInstance.listOfFilesToBeUploaded[indexPath.row]["fileName"] as! NSString).description
                let cell = self.tableView.cellForRowAtIndexPath(indexPath) as! MeetingTableViewCell
                cell.viewTranscript.hidden = true
                cell.uploadTextLabel.hidden = true
                let uploadTask = DataUploadTask()
                uploadTask.sendRecordedData(indexPath.row, fileName: fileName, cell: cell)
 
        } else if (sender.titleLabel?.text == "View Transcript") {
            let fetchTask = FetchHttpTask()
            fetchTask.getTranscript(indexPath.row,
                                    fileName: (DataModel.sharedInstance.listOfFilesToBeUploaded[indexPath.row]["fileName"] as! NSString).description)
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showTranscriptSegue" {
            let destinationViewController = segue.destinationViewController as! TranscriptViewController
            destinationViewController.index = transcriptIndex
        }
    }
    
    func methodOFReceivedNotication(notification: NSNotification){
        let uInfo = notification.userInfo
        let ind = uInfo!["transcriptIndex"]
        transcriptIndex = (ind as! NSNumber).integerValue
        self.performSegueWithIdentifier("showTranscriptSegue", sender: self)
        
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self,
                                                            name: "ShowTranscript",
                                                            object: nil)
    }
    
}
