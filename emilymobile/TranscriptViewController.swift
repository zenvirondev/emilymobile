//
//  TranscriptViewController.swift
//  emilymobile
//
//  Created by pbanavara on 07/04/16.
//  Copyright © 2016 pbanavara. All rights reserved.
//

import UIKit
import AVFoundation

class TranscriptViewController: UIViewController, UISearchBarDelegate, UIGestureRecognizerDelegate {
    lazy var searchBar = UISearchBar(frame: CGRectMake(0, 0, 0, 0))
    var searchActive : Bool = false
    var index:Int!
    
    var audioPlayer:AVAudioPlayer!
    var timer:NSTimer!
    var startTime:NSTimeInterval!
    
    
    @IBOutlet weak var transcriptView: UITextView!
    @IBOutlet weak var sliderValue: UISlider!
    @IBOutlet weak var trascriptText: UITextView!
    
    var timeStamps: [[AnyObject]]!
    var confidence: [[AnyObject]]!
    var lowCofidenceWords: [String] = []
    
    var pauseButton = UIBarButtonItem()
    var playButton = UIBarButtonItem()
    var arrayOfButtons = [UIBarButtonItem]()
    var tapTerm:UITapGestureRecognizer = UITapGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        do {
            
            let shareButton: UIBarButtonItem = UIBarButtonItem.init(barButtonSystemItem:.Action, target: self, action: Selector("userDidTapShare"))
            playButton = UIBarButtonItem.init(barButtonSystemItem:.Play, target: self, action: Selector("playAudio"))
            pauseButton = UIBarButtonItem.init(barButtonSystemItem:.Pause, target: self, action: Selector("pauseAudio"))
            
            
            self.navigationItem.rightBarButtonItems = [shareButton, playButton]
            
            timeStamps = DataModel.sharedInstance.listOfFilesToBeUploaded[index]["timestamps"] as! [[AnyObject]]
            confidence = DataModel.sharedInstance.listOfFilesToBeUploaded[index]["word_confidence"] as! [[AnyObject]]
            let session = AVAudioSession.sharedInstance()
            try! session.setCategory(AVAudioSessionCategoryPlayAndRecord, withOptions: AVAudioSessionCategoryOptions.DefaultToSpeaker)
            searchBar.delegate = self
            
            trascriptText.tintColor = UIColor.blackColor()
            searchBar.placeholder = "Search"
            navigationItem.titleView = searchBar
            trascriptText.scrollEnabled = false
            let text = (DataModel.sharedInstance.listOfFilesToBeUploaded[index]["fileTranscript"] as! NSString).description
            trascriptText.text = text
            
            highlightWord()
            tapTerm = UITapGestureRecognizer(target: self, action: "tapTextView:")
            tapTerm.delegate = self
            trascriptText.addGestureRecognizer(tapTerm)
            
            let fileName = (DataModel.sharedInstance.listOfFilesToBeUploaded[index]["fileName"] as! NSString).description
            let documentsDirectory = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
            let audioURL = documentsDirectory.URLByAppendingPathComponent(fileName)
            audioPlayer = try AVAudioPlayer(contentsOfURL: audioURL)
            sliderValue.maximumValue = Float(audioPlayer.duration)
            
            
        } catch _ as NSError {
            print ("Error in loading")
        }
        
    }
    
    func tapTextView(sender:UIGestureRecognizer) {
        print("tapped here \(sender.locationInView(transcriptView))")
        
        if let textView = sender.view as? UITextView {
            let layoutManager = textView.layoutManager
            var location: CGPoint = sender.locationInView(textView)
            location.x -= textView.textContainerInset.left
            location.y -= textView.textContainerInset.top
            
            var charIndex = layoutManager.characterIndexForPoint(location, inTextContainer: textView.textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
            
            if charIndex < textView.textStorage.length {
                var len = 0
                var range = NSRange(location: charIndex, length:1)
                var tappedPhrase = (textView.attributedText.string as NSString).substringWithRange(range)
                while (tappedPhrase != " ") {
                    len += 1
                    range = NSRange(location: charIndex+len, length:1)
                    tappedPhrase = (textView.attributedText.string as NSString).substringWithRange(range)
                }
                range = NSRange(location: charIndex, length:len)
                tappedPhrase = (textView.attributedText.string as NSString).substringWithRange(range)
                print("tapped phrase: \(tappedPhrase)")
                playWordInTimeStamps(tappedPhrase)
                
                var mutableText = textView.attributedText.mutableCopy() as! NSMutableAttributedString
                mutableText.addAttributes([NSForegroundColorAttributeName: UIColor.redColor()], range: range)
                textView.attributedText = mutableText
                
            }
        }
    }
    
    func playWordInTimeStamps(word: String) {
        for t in timeStamps {
            if( word == (t[0] as! String)) {
                sliderValue.value = Float(t[1] as! NSNumber)
                if (audioPlayer != nil) {
                    audioPlayer.currentTime = NSTimeInterval(t[1] as! NSNumber)
                    audioPlayer.prepareToPlay()
                    audioPlayer.play()
                }
                break
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        trascriptText.scrollEnabled = true
    }
    
    @IBAction func sliderValueChanged(sender: UISlider) {
        let roundedValue = round(sender.value)
        
        trascriptText.editable = true
        trascriptText.selectedRange = NSMakeRange(Int(roundedValue),0)
        if (audioPlayer != nil) {
            audioPlayer.stop()
            audioPlayer.currentTime = NSTimeInterval(sliderValue.value)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        }
        
        
    }
    
    func playAudio() {
        do {
            arrayOfButtons = self.navigationItem.rightBarButtonItems!
            arrayOfButtons.removeAtIndex(1) // change index to correspond to where your button is
            arrayOfButtons.insert(pauseButton, atIndex: 1)
            self.navigationItem.rightBarButtonItems = arrayOfButtons
            startTime = try NSTimeInterval(sliderValue.value)
            self.audioPlayer.play()
            timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("updateSlider"), userInfo: nil, repeats: true)
        } catch {
            print(error)
        }
        
        
    }
    
    func pauseAudio() {
        do {
            arrayOfButtons = self.navigationItem.rightBarButtonItems!
            arrayOfButtons.removeAtIndex(1) // change index to correspond to where your button is
            arrayOfButtons.insert(playButton, atIndex: 1)
            self.navigationItem.rightBarButtonItems = arrayOfButtons
            try self.audioPlayer.pause()
            timer.invalidate()
        } catch {
            print(error)
        }
        
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        highlightText(searchText)
        
    }
    
    func highlightText(searchString: String) {
        let baseString = trascriptText.text
        let attributedText = trascriptText.attributedText.mutableCopy() as! NSMutableAttributedString
        // 2
        let attributedTextRange = NSMakeRange(0, attributedText.length)
        
        attributedText.removeAttribute(NSBackgroundColorAttributeName, range: attributedTextRange)
        do {
            let regex = try NSRegularExpression(pattern: searchString, options: .CaseInsensitive)
            let matches = regex.matchesInString(baseString, options: [], range: NSMakeRange(0, baseString.characters.count))
            for match in matches as [NSTextCheckingResult] {
                let r = match.range
                attributedText.addAttribute(NSBackgroundColorAttributeName, value: UIColor.yellowColor(), range: r)
                
            }
            trascriptText.attributedText = attributedText.copy() as! NSAttributedString
            
        } catch {
            print(error)
        }
    }
    
    func highlightWord() {
        let baseString = trascriptText.text
        let attributedText = trascriptText.attributedText.mutableCopy() as! NSMutableAttributedString
        // 2
        let attributedTextRange = NSMakeRange(0, attributedText.length)
        attributedText.removeAttribute(NSBackgroundColorAttributeName, range: attributedTextRange)
        
        for t in confidence {
            let ts = Double(t[1] as! NSNumber)
            if (ts <= ConfigParams.CONFIDENCE_THRESHOLD) {
                do {
                    let word = t[0] as! String
                    if (word != "the" && word != "of" && word != "an" && word != "is" && word != "was" && word != "a" &&
                        word != "no" && word != "we") {
                        let regex = try NSRegularExpression(pattern: "\\b\(word)\\b", options: .CaseInsensitive)
                        let matches = regex.matchesInString(baseString, options: [], range: NSMakeRange(0, baseString.characters.count))
                        for match in matches as [NSTextCheckingResult] {
                            lowCofidenceWords.append(word)
                            let r = match.range
                            attributedText.addAttribute(NSBackgroundColorAttributeName, value: UIColor.yellowColor(), range: r)
                            
                        }
                    }
                    
                } catch {
                    print(error)
                }
                
            }
        }
        trascriptText.attributedText = attributedText.copy() as! NSAttributedString
        
    }
    
    func updateSlider() {
        sliderValue.value = Float(audioPlayer.currentTime)
        if (audioPlayer.currentTime == 0.0) {
            timer.invalidate()
        }
        
    }
    
    deinit {
        if (timer != nil) {
            timer.invalidate()
        }
        
    }
    
    override func viewDidDisappear(animated: Bool) {
        if (timer != nil) {
            timer.invalidate()
        }
        
    }
    
    func userDidTapShare() {
        let vc = UIActivityViewController(activityItems: [trascriptText.text], applicationActivities: [])
        presentViewController(vc, animated: true, completion: nil)
        
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
}
