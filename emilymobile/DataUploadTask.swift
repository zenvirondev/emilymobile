//
//  DataUploadTask.swift
//  emilymobile
//
//  Created by pbanavara on 13/04/16.
//  Copyright © 2016 pbanavara. All rights reserved.
//

import Foundation
import AVKit
import UIKit

class DataUploadTask: NSObject, NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate {
    
    var currentTableCell:MeetingTableViewCell!
    var elementIndex:Int!
    
    func sendRecordedData(index: Int, fileName: String, cell: MeetingTableViewCell) {
        self.elementIndex = index
        currentTableCell = cell
        let sendToURL = NSURL(string: ConfigParams.URL)
        print(sendToURL)
        
        let documentsDirectory = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
       
        let soundFileURL = documentsDirectory.URLByAppendingPathComponent(fileName)
        print("File location ::: \(soundFileURL)")
        let recording:NSData? = NSData(contentsOfURL: soundFileURL)
        let boundary = "--------14737809831466499882746641449----"
        let beginningBoundary = "--\(boundary)"
        let endingBoundary = "--\(boundary)--"
        let contentType = "multipart/form-data;boundary=\(boundary)"
        
        let body = NSMutableData()
        body.appendData(("\(beginningBoundary)\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData(("Content-Disposition: form-data; name=\"userName\"\r\n\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData(("\(ConfigParams.EMAIL_ID)\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)!)
        
        body.appendData(("\(beginningBoundary)\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData(("Content-Disposition: form-data; name=\"fileName\"\r\n\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData(("\(fileName)\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)!)
        
        body.appendData(("\(beginningBoundary)\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData(("Content-Disposition: form-data; name=\"fileBinary\"; filename=\"\(fileName)\"\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData(("Content-Type: application/octet-stream\r\n\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)!)
        body.appendData(recording!) // adding the recording here
        body.appendData(("\r\n\(endingBoundary)\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)!)
        
        let request = NSMutableURLRequest(URL:sendToURL!)
        request.HTTPMethod = "POST"
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        request.HTTPBody = body
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: configuration, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        let task = session.uploadTaskWithRequest(request, fromData: body)
        task.resume()
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        
        print("Bytes Sent::: \(totalBytesSent)")
        let uploadProgress = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
        let progressPercent = Int(uploadProgress*100)
        if (progressPercent > 0 && progressPercent < 100) {
            if (currentTableCell != nil) {
                currentTableCell.uploadTextLabel.hidden = false
                currentTableCell.uploadProgressView.hidden = false
                //currentTableCell.viewTranscript.enabled = false
                currentTableCell.uploadTextLabel.text = "\(progressPercent)% Done"
                currentTableCell.uploadProgressView.progress = uploadProgress
                //currentTableCell.viewTranscript.enabled = false
            }
            
        }else {
            if (currentTableCell != nil) {
                currentTableCell.viewTranscript.enabled = true
                currentTableCell.uploadProgressView.hidden = true
            }
            
        }
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveResponse response: NSURLResponse, completionHandler: (NSURLSessionResponseDisposition) -> Void) {
        print("Response :::: \(response)")
        completionHandler(NSURLSessionResponseDisposition.Allow)
        
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveData data: NSData) {
        currentTableCell.uploadTextLabel.hidden = true
        currentTableCell.viewTranscript.hidden = false
        currentTableCell.viewTranscript.setTitle("View Transcript", forState: .Normal)
        let newString:NSString = NSString.init(bytes: data.bytes, length: data.length, encoding: NSUTF8StringEncoding)!
        
        do {
            var responseObject : Dictionary<String, String> = try NSJSONSerialization.JSONObjectWithData(newString.dataUsingEncoding(NSUTF8StringEncoding)!, options: []) as! [String:String]
            print(responseObject)
            responseObject["uploadStatus"] = "true"
            DataModel.sharedInstance.listOfFilesToBeUploaded[elementIndex] = responseObject
        } catch let error as NSError {
            print("error: \(error.localizedDescription)")
        }
        
        
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        print("Error ::::: \(error)")
        currentTableCell.viewTranscript.hidden = false
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, willCacheResponse proposedResponse: NSCachedURLResponse, completionHandler: (NSCachedURLResponse?) -> Void) {
        print("Proposed response \(proposedResponse)")
    }


}
