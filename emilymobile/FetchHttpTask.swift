//
//  FetchHttpTask.swift
//  emilymobile
//
//  Created by pbanavara on 13/04/16.
//  Copyright © 2016 pbanavara. All rights reserved.
//

import Foundation

class FetchHttpTask {
    
    func getTranscript(index: Int, fileName: String) {
        let fetchUrl = ConfigParams.FETCH_URL + "?userName=\(ConfigParams.EMAIL_ID)&fileName=\(fileName)"
        let newFetchUrl = NSURL(string: fetchUrl)
        print(newFetchUrl)
        let request = NSMutableURLRequest(URL: newFetchUrl!)
        request.HTTPMethod = "GET"
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {data, response, error in
            if error != nil {
                // handle error here
                print(error)
                return
            }
            do {
                if let responseDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                    print("success == \(responseDictionary)")
                    var indDict = DataModel.sharedInstance.listOfFilesToBeUploaded[index]
                    let trArray:NSMutableArray = []
                    let transcripts = responseDictionary["transcripts"] as! NSArray
                    var timestampArray: NSMutableArray = []
                    var confidenceArray: NSMutableArray = []

                    for t in transcripts {
                        let tDict = t as! NSDictionary
                        trArray.addObject(tDict["transcript"]!)
                        var ts = t["timestamps"] as! [[AnyObject]]
                        var conf = t["word_confidence"] as! [[AnyObject]]
                        for time in ts {
                            timestampArray.addObject(time)
                        }
                        for c in conf {
                            confidenceArray.addObject(c)
                        }
                    }
                    print(trArray)
                    indDict["fileName"] = fileName
                    indDict["s3url"] = DataModel.sharedInstance.listOfFilesToBeUploaded[index]["s3url"]
                    indDict["fileTranscript"] = trArray.componentsJoinedByString("\n")
                    indDict["timestamps"] = timestampArray
                    indDict["word_confidence"] = confidenceArray
                    DataModel.sharedInstance.listOfFilesToBeUploaded[index] = indDict
                    dispatch_async(dispatch_get_main_queue()) {
                        NSNotificationCenter.defaultCenter().postNotificationName("ShowTranscript", object: nil, userInfo: ["transcriptIndex":index])
                    }
                }
            } catch {
                print(error)
                
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
            }
        }
        task.resume()
    }
}
