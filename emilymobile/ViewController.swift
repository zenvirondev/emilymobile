//
//  ViewController.swift
//  emilymobile
//
//  Created by pbanavara on 14/03/16.
//  Copyright © 2016 pbanavara. All rights reserved.
//

import UIKit
import AVFoundation


class ViewController: UIViewController, AVAudioRecorderDelegate, GIDSignInUIDelegate {
    
    @IBOutlet weak var signInButton: GIDSignInButton!
    var uploadFlag:Bool = false
    var listOfFiles = []
    
    @IBAction func finishedRecording(sender: UIButton) {
        //sendRecordedData()
        if (recorder != nil) {
            self.stopRecording()
            self.uploadFlag = true
            var fileUploadStatusDict:[String:String] = [:]
            fileUploadStatusDict["fileName"] = self.currentFileName
            fileUploadStatusDict["s3url"] = ""
            //DataModel.sharedInstance.listOfFilesToBeUploaded.append(fileUploadStatusDict)
            DataModel.sharedInstance.listOfFilesToBeUploaded.insert(fileUploadStatusDict, atIndex: 0)
            self.performSegueWithIdentifier("listOfMeetingsSegue", sender: ViewController.self)

        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        uploadFlag = false
        currentFileName = nil
        
    }
   
    @IBAction func openMeetings(sender: UIButton) {
        self.performSegueWithIdentifier("listOfMeetingsSegue", sender: self)
    }
    
    var recorder:AVAudioRecorder!
    var soundFileURL:NSURL!
    var meterTimer:NSTimer!
    var currentFileName:String!
    var recordedFiles:NSMutableArray = []
    var pauseFlag = false
    
    
    func stopRecording() {
        print("Stopping recording")
        recorder?.stop()
        meterTimer.invalidate()
        recordButton.setTitle("Record", forState:.Normal)
        recordedFiles.addObject(currentFileName)
        pauseFlag = false
    }
    
    @IBAction func recordAudio(sender: UIButton) {
        
        if(sender.titleLabel?.text == "Record") {
            print("Recording")
            recordButton.setTitle("Pause", forState: .Normal)
            if (pauseFlag == true) {
                recordWithPermission(false)
                pauseFlag = false
            } else {
                recordWithPermission(true)
            }
           
        } else if(recorder.recording) {
            if(recordButton.titleLabel?.text == "Pause") {
                pauseRecording()
            }
            
        }
    }
   
    func pauseRecording() {
        recordButton.setTitle("Record", forState: .Normal)
        recorder?.pause()
        pauseFlag = true
    }
   
    @IBOutlet weak var timeDisplay: UILabel!
    
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var recordedTextDisplay: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signInSilently()
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: "receiveToggleAuthUINotification:",
                                                         name: "ToggleAuthUINotification",
                                                         object: nil)
        
        NSLog("Initialized Swift app...")
        toggleAuthUI()
        
         uploadFlag = false
        let fAllUserData = FetchAllUserData()
        fAllUserData.getData()
        // [END_EXCLUDE]
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        recorder = nil
    }
    
    func updateAudioMeter(timer:NSTimer) {
        if recorder.recording {
            let hr = Int(recorder.currentTime / 3600)
            let min = Int(recorder.currentTime / 60)
            let sec = Int(recorder.currentTime % 60)
            let s = String(format: "%02d:%02d:%02d", hr, min, sec)
            timeDisplay.text = s
            //recorder.updateMeters()
        }
    }
    
    func setupRecorder() {
        let format = NSDateFormatter()
        format.dateFormat="yyyy-MMM-dd-HH:mm"
        print(format)
        currentFileName = "\(format.stringFromDate(NSDate()))." + ConfigParams.AUDIO_EXT
        print(currentFileName)
        
        let documentsDirectory = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        print("Document directory \(documentsDirectory)")
        self.soundFileURL = documentsDirectory.URLByAppendingPathComponent(currentFileName)
        
        if NSFileManager.defaultManager().fileExistsAtPath(soundFileURL.absoluteString) {
            // probably won't happen. want to do something about it?
            print("soundfile \(soundFileURL.absoluteString) exists")
        }
        
        let recordSettings:[String : AnyObject] = [
            AVFormatIDKey: NSNumber(unsignedInt:kAudioFormatMPEG4AAC),
            AVEncoderAudioQualityKey : AVAudioQuality.Max.rawValue,
            AVEncoderBitRateKey : 320000,
            AVNumberOfChannelsKey: 2,
            AVSampleRateKey : 44100.0
        ]
        
        do {
            recorder = try AVAudioRecorder(URL: soundFileURL, settings: recordSettings)
            recorder.delegate = self
            recorder.meteringEnabled = true
            recorder.prepareToRecord() // creates/overwrites the file at soundFileURL
        } catch let error as NSError {
            recorder = nil
            print(error.localizedDescription)
        }
        
    }
    
    func recordWithPermission(setup:Bool) {
        let session:AVAudioSession = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
            // ios 8 and later
            if (session.respondsToSelector("requestRecordPermission:")) {
                AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
                    if granted {
                        print("Permission to record granted")
                        //self.setSessionPlayAndRecord()
                        if setup {
                            self.setupRecorder()
                        }
                        self.recorder.record()
                        self.meterTimer = NSTimer.scheduledTimerWithTimeInterval(0.1,
                            target:self,
                            selector:"updateAudioMeter:",
                            userInfo:nil,
                            repeats:true)
                    } else {
                        print("Permission to record not granted")
                    }
                })
            } else {
                print("requestRecordPermission unrecognized")
            }
 
        } catch {
            print("Audio error")
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "listOfMeetingsSegue" {
            let destinationViewController = segue.destinationViewController as! ListOfMeetingsTableViewController
           
            if (currentFileName != nil) {
                destinationViewController.uploadFlag = true
                destinationViewController.currentFileName = currentFileName
            }
        }
        
    }

    func toggleAuthUI() {
        if (GIDSignIn.sharedInstance().hasAuthInKeychain()){
        // Signed in
            signInButton!.hidden = true
        
        } else {
            signInButton.hidden = false
            NSLog("Google Sign in\niOS Failed")
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self,
                                                            name: "ToggleAuthUINotification",
                                                            object: nil)
    }
    
    @objc func receiveToggleAuthUINotification(notification: NSNotification) {
        if (notification.name == "ToggleAuthUINotification") {
            self.toggleAuthUI()
            if notification.userInfo != nil {
                let userInfo:Dictionary<String,String!> =
                    notification.userInfo as! Dictionary<String,String!>
                NSLog("Status Text :::::: \(userInfo["statusText"])")
                
            }
        }
    }
    
}

