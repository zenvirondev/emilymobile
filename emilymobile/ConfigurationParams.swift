//
//  ConfigurationParams.swift
//  emilymobile
//
//  Created by pbanavara on 13/04/16.
//  Copyright © 2016 pbanavara. All rights reserved.
//

import Foundation

struct ConfigParams {
    static let HOST_NAME = "services.obvio.xyz"
    static let URL = "http://\(HOST_NAME):8080/audioFileUpload"
    static let FETCH_URL = "http://\(HOST_NAME):8080/audioFileTranscript"
    static let FETCH_ALL_URL = "http://\(HOST_NAME):8080/getAllUserData"
    static let AUDIO_EXT = "m4a"
    static let ARRAY_FILE_NAME = "array.txt"
    static var EMAIL_ID = "pradeepbs@gmail.com"
    static let CONFIDENCE_THRESHOLD = 0.4
    

}
