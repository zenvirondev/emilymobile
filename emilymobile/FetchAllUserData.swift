//
//  FetchAllUserData.swift
//  emilymobile
//
//  Created by pbanavara on 18/04/16.
//  Copyright © 2016 pbanavara. All rights reserved.
//

import Foundation

class FetchAllUserData {
    
    func getData() {
        let fetchUrl = ConfigParams.FETCH_ALL_URL + "?userName=\(ConfigParams.EMAIL_ID)"
        let newFetchUrl = NSURL(string: fetchUrl)
        print(newFetchUrl)
        let request = NSMutableURLRequest(URL: newFetchUrl!)
        request.HTTPMethod = "GET"
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {data, response, error in
            if error != nil {
                // handle error here
                print(error)
                return
            }
            do {
                if let responseDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                    //print("success == \(responseDictionary)")
                    let userData = responseDictionary["userData"] as! [Dictionary<String, AnyObject>]
                    DataModel.sharedInstance.listOfFilesToBeUploaded = userData.reverse()
                                    }
            } catch {
                print(error)
                let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
            }
        }
        task.resume()
    }

}
